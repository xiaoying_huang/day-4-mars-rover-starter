package com.afs.tdd;

public enum Command {
    TurnLeft, TurnRight, Move
}
