package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.text.MaskFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MarsRoverTest {

    @Test
    void should_plus_location_y_1_North_when_executeCommand_given_location_North_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction locationDirection = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(1, locationY);
        Assertions.assertEquals(Direction.North, locationDirection);
    }

    @Test
    void should_plus_location_x_1_when_executeCommand_given_location_East_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction locationDirection = location.getDirection();

        Assertions.assertEquals(1, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.East, locationDirection);
    }

    @Test
    void should_subtract_location_y_1_when_executeCommand_given_location_South_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction locationDirection = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(-1, locationY);
        Assertions.assertEquals(Direction.South, locationDirection);
    }

    @Test
    void should_subtract_location_x_1_when_executeCommand_given_location_West_and_command_Move() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction locationDirection = location.getDirection();

        Assertions.assertEquals(-1, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.West, locationDirection);
    }

    @Test
    void should_turn_to_West_when_executeCommand_given_direction_North_and_command_turnLeft() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.West, direction);
    }

    @Test
    void should_turn_to_North_when_executeCommand_given_direction_East_and_command_turnLeft() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.North, direction);
    }

    @Test
    void should_turn_to_East_when_executeCommand_given_direction_South_and_command_turnLeft() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.East, direction);
    }

    @Test
    void should_turn_to_South_when_executeCommand_given_direction_West_and_command_turnLeft() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.South, direction);
    }

    @Test
    void should_turn_to_East_when_executeCommand_given_direction_North_and_command_turnRight() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.East, direction);
    }

    @Test
    void should_turn_to_South_when_executeCommand_given_direction_East_and_command_turnRight() {
        //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.South, direction);
    }

    @Test
    void should_turn_to_West_when_executeCommand_given_direction_South_and_command_turnRight() {
        //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.West, direction);
    }

    @Test
    void should_turn_to_North_when_executeCommand_given_direction_West_and_command_turnRight() {
        //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(0, locationX);
        Assertions.assertEquals(0, locationY);
        Assertions.assertEquals(Direction.North, direction);
    }

    @Test
    void should_change_to_right_location_when_executeMulCommands_given_location_and_several_commands() {
        //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        List<Command> commands = Arrays.asList(Command.Move, Command.TurnLeft, Command.Move, Command.TurnRight);
        marsRover.executeMulCommands(commands);

        //then
        int locationX = location.getLocationX();
        int locationY = location.getLocationY();
        Direction direction = location.getDirection();

        Assertions.assertEquals(-1, locationX);
        Assertions.assertEquals(1, locationY);
        Assertions.assertEquals(Direction.North, direction);

    }
}
