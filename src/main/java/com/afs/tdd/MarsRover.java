package com.afs.tdd;

import java.util.List;
import java.util.stream.Collectors;

public class MarsRover {
    private  Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        Direction direction = this.location.getDirection();
        if(command.equals(Command.Move)){
            switch (direction){
                case North: up(); break;
                case East: right(); break;
                case South: down(); break;
                case West: left(); break;
            }
        }
        if(command.equals(Command.TurnLeft)){
            switch(direction){
                case North: turnDirection(Direction.West); break;
                case East: turnDirection(Direction.North); break;
                case South: turnDirection(Direction.East); break;
                case West: turnDirection(Direction.South); break;
            }
        }
        if(command.equals(Command.TurnRight)){
            switch(direction){
                case North: turnDirection(Direction.East); break;
                case East: turnDirection(Direction.South); break;
                case South: turnDirection(Direction.West); break;
                case West: turnDirection(Direction.North); break;
            }

        }
    }
    public void executeMulCommands(List<Command> commands){
        commands.forEach(command -> executeCommand(command));
    }

    private void turnDirection(Direction direction) {
        this.location.setDirection(direction);
    }

    private void left() {
        this.location.setLocationX(this.location.getLocationX() - 1);
    }

    private void down() {
        this.location.setLocationY(this.location.getLocationY() - 1);
    }

    private void right() {
        this.location.setLocationX(this.location.getLocationX() + 1);
    }

    private void up() {
        this.location.setLocationY(this.location.getLocationY() + 1);
    }

}
